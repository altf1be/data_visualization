Data models
===========

How should you design your data model? Which level of details do you plan?


Data model
----------

.. csv-table:: Data models
    :header: "Record name"
    :widths: auto
    
    Total world statistics
    Cases by country
    Cases by country by day
    Data for all countries
    Mask usage instructions
    Infection histories by country
    India-specific figures
    North American figures
    Data by ISO code
    Testing statistics
    Transportation infection data

Multilingual applications
-------------------------

Twenty percent of the population understand English, sharing a dashboard in English describing an outbreak is of little help for the vast majority of the popuplation.

.. note:: 
    Build a multilanguage application from day 1.

[WikiLangSpok20]_ indicates an approximate list of languages by the total number of speakers.

.. csv-table:: List of languages by total number of speakers
    :header: "Rank", "Language", "Speakers (millions)"
    :widths: auto

    1,English,1.268 
    2,Mandarin Chinese,1.120
    3,Hindi,637.2
    4,Spanish,537.9
    5,French,276.6
    6,Standard Arabic,274.0
    7,Bengali,265.2
    8,Russian,258.0
    9,Portuguese,252.2
    10,Indonesian,199.0
    11,Urdu,170.6
    12,German,131.6
    13,Japanese,126.4
    14,Swahili,98.5
    15,Marathi,95.3


List of locations
-----------------

List of locations consist of several information; e.g., country name, state, county, province, commune, longitude, latitude, post box

Bing lists countries and states names in a text file; US States are separated by a slash '/'
    https://github.com/microsoft/COVID-19-Widget/blob/master/AllLocations.txt
