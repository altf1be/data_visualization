Projects
========


Comparison of COVID-19 case reporting from different sources
------------------------------------------------------------

Daily cumulative case numbers (starting Jan 22, 2020) reported by the Johns Hopkins University Center for Systems Science and Engineering (CSSE), WHO situation reports, and the Chinese Center for Disease Control and Prevention (Chinese CDC) for within (A) and outside (B) mainland China.

Source : An interactive web-based dashboard to track COVID-19 in real time: https://www.thelancet.com/journals/laninf/article/PIIS1473-3099%2820%2930120-1/fulltext
