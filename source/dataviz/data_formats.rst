Data formats
============

How to format the data your are exposing: text, numeric, complex object using XML or JSON?

List of locations in a TXT file
-------------------------------

Source : https://github.com/microsoft/COVID-19-Widget/blob/master/AllLocations.txt

.. csv-table:: All Locations.txt
    :header: "Location: Country/State"
    
    /
    /United States
    /United States/Alabama
    /United States/Alaska
    /Afghanistan
    /Albania
    /Algeria