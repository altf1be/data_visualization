.. "=  - # ` : ' " ~ ^ _ *  < >"

=========================
Introduction
=========================

Hundreds of initiatives have been initiated to display key indicators related to the spread of COVID-19; individuals, non-profit companies, for-profit companies and public administrations published dashboards all over the world.

The dashboards were built by software like Qlik or relied on private or open-source code.

Centers for Disease Control and Prevention became famous : :term:`CDC` in the US, :term:`ECDC` in Europe became overloaded by requests.

Data collected by public administrations were made publicly available and the first dashboards came up with its share of critics.
