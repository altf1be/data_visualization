
.. include:: _header.rst

Widgets anyone can embed into a html page
=========================================



COVID-19 on Bing
----------------

Data is collected from multiple sources (`CDC`_, `WHO`_, `ECDC`_, `Wikipedia`_, `24/7 Wall St.`_, `BNO News`_) that update at different times and may not always align. Some regions may not provide complete breakdown of COVID-19 related stats.
    https://bing.com/covid

    .. literalinclude:: code/widget-bing.com.html
