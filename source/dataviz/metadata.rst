.. include:: _header.rst

Metadata
========

* Hospital

    * All beds needed is the total number of beds needed exclusively for COVID patients, and includes ICU beds needed for COVID patients. `covid19.healthdata.org`_
    * Bed shortage
    * ICU beds needed is the total number of ICU beds needed exclusively for COVID patients. `covid19.healthdata.org`_
    * ICU bed shortage
    * Invasive ventilators needed

* Uncertainty is the range of values that is likely to include the correct projected estimate for a given data category. Larger uncertainty intervals can result from limited data availability, small studies, and conflicting data, while smaller uncertainty intervals can result from extensive data availability, large studies, and data that are consistent across sources. The model presented in this tool has a 95% uncertainty interval and is represented by the shaded area(s) on each chart. `covid19.healthdata.org`_

* correlation between metadata

    * All beds needed -> Bed shortage
    * ICU beds needed -> ICU bed shortage
