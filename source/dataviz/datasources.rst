Datasources
===========

72h nonprofit online hackathon is to develop open-source prototypes, which contribute to solving the most pressing challenges in the current crisis.

    https://www.codevscovid19.org/

Tens of thousands of volunteers build solutions for the Corona pandemic. To avoid reinventing the wheel, we created a central place to learn about existing projects and add new ideas.

    https://airtable.com/shrPm5L5I76Djdu9B/tbl6pY6HtSZvSE6rJ/viwbIjyehBIoKYYt1?blocks=bipjdZOhKwkQnH1tV

Location for summaries and analysis of data related to n-CoV 2019, first reported in Wuhan, China

    https://github.com/beoutbreakprepared/nCoV2019

The Institute for Health Metrics and Evaluation (IHME) is an independent population health research center at UW Medicine, part of the University of Washington, that provides rigorous and comparable measurement of the world's most important health problems and evaluates the strategies used to address them 

    http://www.healthdata.org/  

COVID-19 Projections worldwide 

    https://covid19.healthdata.org/united-states-of-america 

NIH National Institute of Health, Open-Access Data and Computational Resources to Address COVID-19: 

    https://datascience.nih.gov/covid-19-open-access-resources


The COVID Tracking Project collects and publishes the most complete testing data available for US states and territories 

    https://covidtracking.com

Bing COVID-19 data sources 

    https://help.bing.microsoft.com/#apex/18/en-us/10024

A repo for coronavirus related case count data from around the world. The repo will be regularly updated 

    https://github.com/microsoft/Bing-COVID-19-Data 


We are building an open database of COVID-19 cases with chest X-ray or CT images 

    https://github.com/ieee8023/covid-chestxray-dataset 

Data in time of COVID-19 

    https://opendatawatch.com/what-is-being-said/data-in-the-time-of-covid-19/


Hack for Wuhan

    https://github.com/wuhan2020/Hackathon/blob/master/README_EN.md

#WirVsVirus Hackathon

    https://wirvsvirushackathon.org/

    Overview of topics and challenges: https://airtable.com/shrPm5L5I76Djdu9B

    Overview of submissions: https://wirvsvirushackathon.devpost.com/submissions

Data Repository by Johns Hopkins

    https://github.com/CSSEGISandData/COVID-19

Open Source Know-How

    https://www.healthcare-computing.de/know-how-zum-coronavirus-fuer-wissenschaftscommunity-a-909391/

White House Dataset

    https://www.whitehouse.gov/briefings-statements/call-action-tech-community-new-machine-readable-covid-19-dataset/

COVID-19 Open Research Dataset (CORD-19) by Microsoft Research

    Details: https://www.microsoft.com/en-us/research/project/academic/articles/microsoft-academic-resources-and-their-application-to-covid-19-research/

    Access: https://pages.semanticscholar.org/coronavirus-research

COVID-19 Data

    https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/resource/62eb477f-be00-462a-831a-594095f7306a

COVID-19 Open Research Dataset Challenge

    https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge/tasks

Crowdbreaks Data

    https://www.crowdbreaks.org/en/projects/covid

COVID-19 Cases Switzerland

    https://github.com/daenuprobst/covid19-cases-switzerland

    Project of Daniel Probst: https://www.corona-data.ch/

COVID-19 case numbers communicated by official Swiss Canton's and FL's sources

    https://github.com/openZH/covid_19/blob/master/README.md

Swiss Hospital Data

    https://github.com/schoolofdata-ch/swiss-hospital-data

Swiss Federal Railways Data

    Open Data: https://opentransportdata.swiss/ and https://data.sbb.ch/

    Open Journey Planner (OJP): https://opentransportdata.swiss/de/cookbook/open-journey-planner-ojp/

Open Data City of Zurich

    Air Quality: https://data.stadt-zuerich.ch/dataset/luftqualitaet-tages-aktuelle-messungen

    Traffic count data for motorized private transport (hourly values): https://data.stadt-zuerich.ch/dataset/sid_dav_verkehrszaehlung_miv_od2031

    Parking guidance system: https://data.stadt-zuerich.ch/dataset/parkleitsystem
    
Zurich Tourism Open Data

    https://www.zuerich.com/open-data 

Reddit Resources

    https://www.reddit.com/r/MachineLearning/comments/fks234/nd_resources_and_channels_to_help_with_covid19/

Nth Opinion

    https://github.com/nthopinion/covid19


World Health Organization App
    https://github.com/WorldHealthOrganization/app

European CDC Data

    https://www.ecdc.europa.eu/en
 
ACAPS Resources

    https://www.acaps.org/projects/covid19

Real-time tracking of pathogen evolution

    nextstrain.org

COVID Tracker

    https://github.com/sagarkarira/coronavirus-tracker-cli

COVID-19 Hospital Impact Model for Epidemics

    https://github.com/CodeForPhilly/chime

NVIDIA Parabricks Genomics Analysis Toolkit

    https://www.developer.nvidia.com/nvidia-parabricks

Electricity Generation, Transportation and Consumption Data

    https://transparency.entsoe.eu/dashboard/show

COVID-19 API Initiative

    https://www.xapix.io/covid-19-initiative

Facebook API & SDK

    Messenger: https://developers.facebook.com/docs/messenger-platform/

    Instagram & Facebook Stories: https://developers.facebook.com/docs/instagram-api/reference/user/stories/

Proximity Tech to fight COVID-19 by Uepaa

    http://blog.p2pkit.io/proximity-tech-to-fight-covid-19/

Scandit SDK

    For barcode, text and ID document scanning capabilities

    https://www.scandit.com/developers/

Swiss Radio and Television API

    https://developer.srgssr.ch/

    https://developer.srgssr.ch/apis

MongoDB’s flexible data model

    Go and try it for free at https://cloud.mongodb.com and if you require more credits, please fill in this form: https://forms.gle/R4zLtWurWkNFMozq9

IBM Cloud and Watson APIs

    https://ibm.biz/cloud-vs-covid19 Please ping IBM on slack to get a unique Feature Code

    https://developer.ibm.com/blogs/the-2020-call-for-code-global-challenge-takes-on-covid-19/

Postman COVID-19 API Resource Center

    https://covid-19-apis.postman.com/

Google Maps, Cloud and G Suite

    https://docs.google.com/document/d/17eODy-Jq7yPE8PiVXKWK7pUePc4cOOG0YJ-Rhc7sdyg/edit

    Maps: https://developers.google.com/maps/covid19

Fossilo.com - Daily Archives of COVID-19 relevant pages

    https://www.fossilo.com/covid-19-offline

Lockdowns by country
--------------------

COVID-19 Lockdown dates by country; A list of countries and the dates that each country went into lockdown.

    https://www.kaggle.com/jcyzag/covid19-lockdown-dates-by-country

Tests conducted by country
--------------------------

Covid19 Tests Conducted by Country; Captures the number of tests conducted in any country/region

    https://www.kaggle.com/skylord/covid19-tests-conducted-by-country

Flu Tracking
------------

Belgium

    https://www.covid19stop.org

Australia

    https://www.flutracking.net

New Zealand

    https://www.flutracking.net

clinical trials
---------------

Vivli A global clinical research data sharing platform. The Vivli team is dedicated to helping researchers share and access data from clinical trials to advance science 

    https://vivli.org/

.. todo::

    continue à partir de data vizualization