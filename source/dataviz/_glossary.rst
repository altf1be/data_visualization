.. "= - # ` : ' " ~ ^ _ * < >"


Glossary for Data Visualization
===============================

.. glossary::
   :sorted:

   CDC

    Center for Disease Control and Prevention

    https://www.cdc.gov

   ECDC

    European Center for Disease Control and Prevention

    https://ecdc.europa.eu