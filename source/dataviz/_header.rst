.. _24/7 Wall St.: https://247wallst.com

.. _BNO News: https://bnonews.com/index.php/2020/04/the-latest-coronavirus-cases

.. _CDC: https://www.cdc.gov/coronavirus/2019-ncov/index.html

.. _covid19.healthdata.org: https://covid19.healthdata.org

.. _WHO: https://www.who.int/emergencies/diseases/novel-coronavirus-2019

.. _ECDC: https://www.ecdc.europa.eu/en/covid-19-pandemic

.. _Wikipedia: https://en.wikipedia.org/wiki/2019–20_coronavirus_pandemic