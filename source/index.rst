
Data Visualization during outbreaks like COVID-19
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dataviz/introduction.rst
   dataviz/rest_apis.rst
   dataviz/articles.rst
   dataviz/dashboards.rst
   dataviz/datasources.rst
   dataviz/examples.rst
   dataviz/data_formats.rst
   dataviz/data_models.rst
   dataviz/metadata.rst
   dataviz/projects.rst
   dataviz/software_architecture.rst
   dataviz/vocabulary.rst
   dataviz/rda_covid-19_recommendations_and_guidelines.rst
   dataviz/_glossary.rst
   dataviz/_citations.rst
   dataviz/_to_do_list.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
