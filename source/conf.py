# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
import datetime
import unidecode
# sys.path.insert(0, os.path.abspath('.'))


# correct the issue : pdf generation fails after a 'libpng error: Not a PNG file'
# See https://github.com/readthedocs/readthedocs.org/issues/6770

# gist source: https://gist.github.com/Abdelkrim/c1d7005feccbcfa66b198151dc1c4abd

# -- manage git-lfs ----------------------------------------------------------

import os
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

if on_rtd:
    if not os.path.exists('./git-lfs'):
        os.system('wget https://github.com/git-lfs/git-lfs/releases/download/v2.7.1/git-lfs-linux-amd64-v2.7.1.tar.gz')
        os.system('tar xvfz git-lfs-linux-amd64-v2.7.1.tar.gz')
        os.system('./git-lfs install')  # make lfs available in current repository
        os.system('./git-lfs fetch')  # download content from remote
        os.system('./git-lfs checkout')  # make local files to have the real content on them

# -- Project information -----------------------------------------------------

project = 'Data Visualization during an outbreak like COVID-19'

# projectname = 'ALT-F1.be-' + project.replace(' ', '_')

projectname = 'ALT-F1.BE-' + unidecode.unidecode(
    project.replace(' ', '_')
)
projectname_with_spaces = project  # + ' by ALT-F1'


copyright = u'(c) 2010-2020 Abdelkrim Boujraf '\
            u'ALT-F1 SPRL <http://www.alt-f1.be>'
author = 'Abdelkrim B.'

# The short X.Y version
version = u'v' + datetime.datetime.today().strftime('%Y.%m.%d')
# The full version, including alpha/beta/rc tags
release = version + ' ' + datetime.datetime.today().strftime('%H.%M')


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_rtd_theme",
    "sphinx.ext.todo",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The master toctree document.
master_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_*.rst']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {
    #     'canonical_url': '',
    #     'analytics_id': '',
    #     'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'both',
    'style_external_links': True,
    #     #'vcs_pageview_mode': '',
    #     # Toc options
    'collapse_navigation': True,
    #     'sticky_navigation': True,
    'navigation_depth': 4,
    #     'includehidden': True,
    #     'titles_only': False
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for LaTeX output ------------------------------------------------
f = open('../_scripts/latex-styling.tex', 'r+')
PREAMBLE = f.read()

latex_elements = {
    # https://www.sphinx-doc.org/en/master/latex.html
    # 'classoptions': ',openany,twoside',
    'classoptions': ',openany',

    # 'babel' : '\\usepackage[french]{babel}',
    # The paper size ('letterpaper' or 'a4paper').
    #
    'papersize': 'a4paper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',
    'preamble': PREAMBLE,
    'maxlistdepth': '10',
    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
    'figure_align': 'H',
}

latex_documents = [
    (master_doc, projectname + '.tex', projectname_with_spaces,
     author, 'manual'),
]


def setup(app):
    app.add_stylesheet("css/custom.css")


# -- Options for todo extension ----------------------------------------------
# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
todo_emit_warnings = True
