# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* best practices to perform data visualization during COVID-19 outbreak
* Version

### How do I get set up? ###

* Build the documentation
    * `conda activate pandas`
    * `make html`| `make latexpdf`
* Read the document
    * on ubuntu `xdg-open build/html/index.html`
    * on ubuntu `xdg-open build/html/ALT-F1.BE-Data_Visualization_during_an_outbreak_like_COVID-19.pdf`
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Correct misspellings

* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact